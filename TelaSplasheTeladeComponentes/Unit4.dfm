object Form4: TForm4
  Left = 189
  Top = 205
  Width = 952
  Height = 461
  Caption = 'Win32 e System'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 944
    Height = 40
    Align = alTop
    Alignment = taCenter
    Caption = 'Paleta Win32 e System'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -35
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object TabControl1: TTabControl
    Left = 0
    Top = 40
    Width = 944
    Height = 390
    Align = alClient
    DragCursor = crHourGlass
    TabOrder = 0
    Tabs.Strings = (
      'www.cedupcriciuma.com/WebNode'
      'FrivGamesOnline.com'
      'TheWalkingDeadOnline')
    TabIndex = 0
    object Button1: TButton
      Left = 256
      Top = 312
      Width = 329
      Height = 41
      Caption = 'Carregar Barra de Progresso'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 608
      Top = 280
      Width = 289
      Height = 49
      Caption = 'Fechar Programa'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -23
      Font.Name = 'Terminator Two'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object PageControl1: TPageControl
    Left = 48
    Top = 80
    Width = 289
    Height = 193
    MultiLine = True
    TabOrder = 1
  end
  object RichEdit1: TRichEdit
    Left = 360
    Top = 88
    Width = 185
    Height = 89
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Lines.Strings = (
      'Esse '#233' um '
      'RichEdit1')
    ParentFont = False
    TabOrder = 2
  end
  object TrackBar1: TTrackBar
    Left = 8
    Top = 72
    Width = 53
    Height = 201
    Orientation = trVertical
    TabOrder = 3
  end
  object ProgressBar1: TProgressBar
    Left = 264
    Top = 304
    Width = 297
    Height = 41
    TabOrder = 4
  end
  object MonthCalendar1: TMonthCalendar
    Left = 576
    Top = 144
    Width = 191
    Height = 154
    CalColors.BackColor = clMenuText
    CalColors.TextColor = clNone
    CalColors.TitleBackColor = clLime
    CalColors.TitleTextColor = clActiveCaption
    CalColors.MonthBackColor = clGreen
    Date = 43328.703536087960000000
    TabOrder = 5
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 704
    Top = 64
  end
  object XPManifest1: TXPManifest
    Left = 824
    Top = 24
  end
end
