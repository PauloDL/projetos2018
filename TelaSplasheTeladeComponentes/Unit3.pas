unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, StdCtrls, Mask, Buttons, ValEdit, jpeg;

type
  TForm3 = class(TForm)
    StaticText1: TStaticText;
    BitBtn1: TBitBtn;
    SpeedButton1: TSpeedButton;
    MaskEdit1: TMaskEdit;
    StringGrid1: TStringGrid;
    DrawGrid1: TDrawGrid;
    Image1: TImage;
    StaticText2: TStaticText;
    ValueListEditor1: TValueListEditor;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses Unit4;

{$R *.dfm}

procedure TForm3.Button1Click(Sender: TObject);
begin
  Form4.ShowModal;
  Form3.Hide;
end;

end.
