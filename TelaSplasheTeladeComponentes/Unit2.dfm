object Form2: TForm2
  Left = 216
  Top = 184
  Width = 803
  Height = 480
  Caption = 'Standard'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 795
    Height = 35
    Align = alTop
    Alignment = taCenter
    Caption = 'Paleta Standard'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -35
    Font.Name = 'Terminator Two'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 16
    Top = 48
    Width = 121
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Text = 'Edit1'
  end
  object Memo1: TMemo
    Left = 16
    Top = 88
    Width = 185
    Height = 89
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    Lines.Strings = (
      'Ap'#243's ver todos os '
      'Componentes, v'#225' '
      'em "Menu=>Tela 2"')
    ParentFont = False
    TabOrder = 1
  end
  object Button1: TButton
    Left = 224
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Button1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
  end
  object CheckBox1: TCheckBox
    Left = 232
    Top = 128
    Width = 129
    Height = 17
    Caption = 'CheckBox1'
    Checked = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    State = cbChecked
    TabOrder = 3
  end
  object RadioButton1: TRadioButton
    Left = 232
    Top = 160
    Width = 137
    Height = 17
    Caption = 'RadioButton1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object ListBox1: TListBox
    Left = 376
    Top = 80
    Width = 185
    Height = 97
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -21
    Font.Name = 'Times New Roman'
    Font.Style = []
    ItemHeight = 23
    Items.Strings = (
      'Esse '#233' um ListBox')
    ParentFont = False
    TabOrder = 5
  end
  object ComboBox1: TComboBox
    Left = 240
    Top = 256
    Width = 145
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ItemHeight = 24
    ParentFont = False
    TabOrder = 6
    Text = 'ComboBox1'
    Items.Strings = (
      'Deveria ter Algo Aqui neh?')
  end
  object ScrollBar1: TScrollBar
    Left = 256
    Top = 208
    Width = 121
    Height = 17
    PageSize = 0
    TabOrder = 7
  end
  object GroupBox1: TGroupBox
    Left = 416
    Top = 200
    Width = 185
    Height = 105
    Caption = 'GroupBox1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
  end
  object RadioGroup1: TRadioGroup
    Left = 32
    Top = 200
    Width = 185
    Height = 105
    Caption = 'RadioGroup1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Items.Strings = (
      'Eu sou um Merda'
      '2-51 '#233' tudo TwoFaces'
      'Tudo babaca')
    ParentFont = False
    TabOrder = 9
  end
  object Panel1: TPanel
    Left = 610
    Top = 35
    Width = 185
    Height = 394
    Align = alRight
    Caption = 'Esse '#233' um Panel1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 10
  end
  object MainMenu1: TMainMenu
    Left = 144
    Top = 48
    object Menu1: TMenuItem
      Caption = 'Menu'
      object ela21: TMenuItem
        Caption = 'Tela2'
        OnClick = ela21Click
      end
    end
  end
end
