unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
Var       VarStr : String;
          ValInt : Integer;
          ValRea : Real;
begin
          VarStr := 'JuceGod';
          ValInt :=  101010;
          ValRea :=  1010.10;
          ShowMessage ('Mensagem Composta de algaritimos complexos para a humanidade');
          ShowMessage (Button1.Caption);
          ShowMessage (VarStr); // N�o necess�rio Converter
          ShowMessage (IntToStr(ValInt)); // Necess�rio Converter
          ShowMessage (FloatToStr(ValRea)); // Necess�rio Converter
          //Concatena��o (Delphi so concatena String)
          ShowMessage(VarStr
                      +CHR(13)
                      +IntToStr(ValInt)
                      +CHR(13)
                      +FloatToStr(ValRea) );
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
         // MessageDLG (String, Tipo, Botoes ou grupo,Help);
         // Obs.: Qdo for botoes individuais devem estar dentro de colchetes
         MessageDlg ('O SEU PC ACABOU DE EXPLODIR!!!!!!!!!!!!!!!!!!!!!!!!!!',
                      mtError, [mbOk],  0);
         MessageDlg ('N�O TEM MAIS VOLTA',
                      mtInformation, mbAbortRetryIgnore,  0);
         MessageDlg ('VOC� DEVERIA COMPRAR UM PC NOVO DA AMD DE PREFERENCIA, ELES S�O MUITO MELHORES QUE INTEL',
                      mtInformation, [mbOk],  0);
         MessageDlg ('Deseja compra-lo agora?',
                      mtConfirmation, mbYesNoCancel,  0);
         MessageDlg ('Parabeins', mtInformation,
                      [mbYes, mbNo, mbOK, mbCancel, mbHelp, mbAbort, mbRetry, mbIgnore, mbAll],
                        0);
end;

procedure TForm1.Button5Click(Sender: TObject);
Var       Resposta : Word;
begin
          Resposta := MessageDLG ('A pagina XVideos n�o est� respondendo. Recarregar?',
                      mtInformation,[mbYes,mbNo],0);

         If Resposta = mrYes
            Then ShowMessage ('Recarregando pagina')      // mrYes
            Else Begin                                       // mrNo
                    Form1.Color := clBlue;
                    ShowMessage ('Famoza Tela Blue');
                 End;
{
        // Com o Comando Case (+ 2 botoes ...)
         Case Resposta Of
           mrYes: ShowMessage ('Regravando os dados ...');
           mrNo:  Begin
                     Form1.Color := clRed;
                     ShowMessage ('Analise os dados e repita o processo');
                  End;
          Else //... Use o else quando nao for nenhum dos bot�es
         End;
 }



end;

procedure TForm1.Button6Click(Sender: TObject);
begin     //MessageDlgPOS (String, Tipo, Botoes, Help, X, Y);
          MessageDlgPOS ('Sua tela est� azul? Podemos resolver',
                         mtInformation, [mbYes,mbNo], 0, 20, 20);
          if Form1.Color = clBlue
          then  Form1.Color := clBtnFace
          else begin
                ShowMessage ('N�o tem nada que possamos fazer')
          end;
end;

procedure TForm1.Button7Click(Sender: TObject);
Var       Resposta : String;
begin
          // InputBox ('Titulo', 'Mensagem', 'Default')
          Resposta := InputBox ('Titulo da caixa de questionamento',
                                'Favor fornece seu email',
                                'minecraftgamer@hotmail.com');

          ShowMessage ('Enviando email para ... [ ' +
                        Resposta + ' ]');
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
          MessageDLG ('Bot�es que funcionam',mtInformation,[mbYes, mbNo, mbOk, mbCancel],0);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
          MessageDLG ('hiper.cool est� passando por problemas GRAVES, volte mais tarde', mtError, [mbOk], 0);
end;

end.
