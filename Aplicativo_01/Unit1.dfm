object Form1: TForm1
  Left = 872
  Top = 143
  Width = 496
  Height = 204
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 8
    Width = 401
    Height = 29
    Alignment = taCenter
    AutoSize = False
    Caption = 'Alistamento Militar'
    Color = clYellow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Layout = tlCenter
  end
  object Label2: TLabel
    Left = 40
    Top = 48
    Width = 89
    Height = 25
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Nome:'
    Color = clYellow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Layout = tlCenter
  end
  object Label3: TLabel
    Left = 40
    Top = 80
    Width = 89
    Height = 25
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Sexo:'
    Color = clYellow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Layout = tlCenter
  end
  object Label4: TLabel
    Left = 40
    Top = 112
    Width = 89
    Height = 25
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Ano Nasc:'
    Color = clYellow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Layout = tlCenter
  end
  object Edit1: TEdit
    Left = 136
    Top = 48
    Width = 305
    Height = 25
    TabOrder = 0
    Text = 'Edit1'
  end
  object MaskEdit1: TMaskEdit
    Left = 136
    Top = 80
    Width = 120
    Height = 25
    EditMask = 'L;1;_'
    MaxLength = 1
    TabOrder = 1
    Text = ' '
  end
  object MaskEdit2: TMaskEdit
    Left = 136
    Top = 112
    Width = 120
    Height = 25
    EditMask = '9999;1;_'
    MaxLength = 4
    TabOrder = 2
    Text = '    '
  end
  object Button1: TButton
    Left = 264
    Top = 80
    Width = 177
    Height = 57
    Caption = 'Verifica'
    TabOrder = 3
    OnClick = Button1Click
  end
end
